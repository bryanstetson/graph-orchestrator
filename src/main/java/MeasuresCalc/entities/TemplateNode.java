package MeasuresCalc.Entities;

import MeasuresCalc.Application;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.Transient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents a static template node outlining the ordering of dependencies
 */
@NodeEntity
public class TemplateNode {
    private final static Logger log = LoggerFactory.getLogger(TemplateNode.class);

    @GraphId
    private Long id;
    private String name;
    private String driver;
    private String version;

    @Transient
    private List<Integer> providerIds;
    @Transient
    private String status;

    @Relationship(type = "Depends On", direction = Relationship.OUTGOING)
    public Set<TemplateNode> dependencies;

    private TemplateNode() {
        // Empty constructor required as of Neo4j API 2.0.5
    }
    public TemplateNode(String name) {
        this.name = name;
    }

    public TemplateNode(String name, String version) {
        this.name = name;
        this.version = version;
    }

    public void processNode(){
        log.info("Start node " + this.name);
        if(dependencies == null || dependencies.isEmpty()){
            processDriver();
        } else {
            for(TemplateNode t : dependencies){
                //TODO: create a multithreaded way of working on multiple dependencies at the same time
                t.processNode();
            }
            processDriver();
        }


    }

    //Iteration 1: wait constant amount of time, same results every run
    //Iteration 2: wait a variable amount of time, see different results occasionally
    public void processDriver(){
        log.info("Driver processing: " + name);
        //Wait
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    public void dependsOn(TemplateNode node) {
        if (dependencies == null) {
            dependencies = new HashSet<>();
        }
        dependencies.add(node);
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public List<Integer> getProviderIds() {
        return providerIds;
    }

    public void setProviderIds(List<Integer> providerIds) {
        this.providerIds = providerIds;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String toString() {

        return this.name + "'s dependencies => "
                + Optional.ofNullable(this.dependencies).orElse(
                Collections.emptySet()).stream().map(
                templateNode -> templateNode.getName()).collect(Collectors.toList());
    }
}
