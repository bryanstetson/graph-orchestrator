package MeasuresCalc.Entities;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import java.util.List;
import java.util.Map;

public interface TemplateNodeRepository extends GraphRepository<TemplateNode>{
    TemplateNode findByName(String name);

    //Note: will only grab 1 level deep, will not recursively grab dependencies
    TemplateNode findByVersionAndName(String name, String version);

    List<TemplateNode> findAll();

    List<TemplateNode> findByVersion(String version);

    @Query("MATCH (n) DETACH DELETE n")
    void wipeDatabase();
}
