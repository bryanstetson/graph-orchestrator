package MeasuresCalc;

import MeasuresCalc.Entities.TemplateNode;
import MeasuresCalc.Entities.TemplateNodeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@EnableNeo4jRepositories(basePackages = "MeasuresCalc/Entities")
public class Application {

    private final static Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner demo(TemplateNodeRepository templateNodeRepository) {
        return args -> {
            String graphVersion = args[0];
            String startNodeName = args[1];

//            templateNodeRepository.wipeDatabase();
//            setupPOC1(templateNodeRepository, graphVersion);
//            setupPOC1(templateNodeRepository, "2");

            List<TemplateNode> nodeList = templateNodeRepository.findByVersion(args[0]);
            TemplateNode startNode = createGraph(nodeList).get(startNodeName);
            if(startNode != null){
                startNode.processNode();
            }

//            List<TemplateNode> nodeList = templateNodeRepository.findAll();
//            TemplateNode startNode = templateNodeRepository.findByVersionAndName(graphVersion, startNodeName);

            log.debug("Echo all nodes");
            nodeList.stream().forEach(node -> log.debug("\t" + node.toString()));
        };
    }

    public static void setupPOC1(TemplateNodeRepository templateNodeRepository, String version){
        TemplateNode A = new TemplateNode("A", version);
        TemplateNode B = new TemplateNode("B", version);
        TemplateNode C = new TemplateNode("C", version);
        TemplateNode D = new TemplateNode("D", version);
        TemplateNode E = new TemplateNode("E", version);
        TemplateNode F = new TemplateNode("F", version);

        List<TemplateNode> team = Arrays.asList(A, B, C, D, E, F);
        log.debug("Before linking up with Neo4j...");
        team.stream().forEach(node -> log.debug("\t" + node.toString()));

        A.dependsOn(B);
        A.dependsOn(C);
        templateNodeRepository.save(A);

        B.dependsOn(D);
        B.dependsOn(E);
        templateNodeRepository.save(B);

        E.dependsOn(F);
        templateNodeRepository.save(E);

        templateNodeRepository.save(A);
        templateNodeRepository.save(B);
        templateNodeRepository.save(C);
        templateNodeRepository.save(D);
        templateNodeRepository.save(E);
        templateNodeRepository.save(F);
    }

    public Map<String, TemplateNode> createGraph(List<TemplateNode> nodeList){
        Map<String, TemplateNode> graph = new HashMap<>();
        nodeList.stream().forEach(node -> graph.put(node.getName(), node));
        return graph;
    }
}
